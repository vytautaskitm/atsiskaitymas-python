prekes = ['pienas', 'kefyras','duona', 'mesyte']
Kainos = [      1,  1.1,   0.8,     2.5]
# eil. nr.:    0      1     2        3
prekes_kainos { 'pienas':1,
                'kefyras':1.1,
                'duona':0.8,
                'mesyte':2.5
                }
                
suma = 0
pirkejo_pinigai = 3 
krepselis = []

while True:
    ka = input("Ką perkam: ")
    if ka == "nieko":
        break

    if ka not in Prekes:
       print(ka, "neturim, bet yra:", Prekes )
       continue  
     
    nr = Prekes.index(ka) 
    kaina = Kainos[nr]
    
    print(ka, "kainuoja", kaina)
    
    suma += kaina
    krepselis.append(ka)

print("Pirkiniai: ", krepselis)
print("bendra suma: ", suma)