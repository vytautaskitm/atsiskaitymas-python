# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# This is a sample controller
# this file is released under public domain and you can use without limitations
# -------------------------------------------------------------------------
from random import randint

from datetime import datetime 
import random

def bot_note(note):
    
    if note.endswith("?"):
        return "nežinau.."
        # return random.choice(["nežinau...", 'gali būt', 'emm...'])
    else:
        zodziai = note.split()
        iterp = "tai gal turbūt manau yra".split()
        ats = ""
        for z in zodziai:
            ats += random.choice(iterp)+" "+ z +" "
        return ats

@auth.requires_login()
def post():
    # return BEAUTIFY(request)
    try: 
        id = int(request.args[0])  # pabandom gaut post'o ID (iš url)
    except:
        id = None  # jei ne - kursime naują post'ą

    # 
    #   SQLFORM  -- labai  mandra funkcija -- sukuria formą pagal lentelės aprašą
    #
    #    http://web2py.com/books/default/chapter/29/07/forms-and-validators?search=sqlform#SQLFORM
    

    post = db.post[id]
    form = SQLFORM(db.post, id, deletable=True)  

    if form.process().accepted:
        info  = 'form accepted'
        redirect( URL(f='list_posts'))
    elif form.errors:
        info = 'form has errors'
    else:
        info = 'please fill out the form'
        

    if  id == None:
        form = DIV(form, info)  # nauja forma 

    elif post.author == auth.user.id:   # redagavimo forma
        form = DIV(form, info)
    else:
        form = None
    

    return { 
            'form': form,   # pagrindiniai reikalai 
            'post': post,
            'comments': LOAD(f='notes', args=[id],  ajax=True, ajax_trap=True) # komentarai
            # http://web2py.com/books/default/chapter/29/12/components-and-plugins?search=components#LOAD-signature
              
            }

def list_posts():
    rows =  db( ).select( db.post.title, db.post.author, db.post.id, db.post.image  )
    # rows = [x.id  for x in rows ]
    rows = [ 
             LI( 
                 A(x.title, _href=URL('post/%s' % x.id))  , 
                 IMG(_src=URL('download', args=x.image))
             )
             for x in rows 
           ]

    rows.append( A("..new..", _href="post"))  # pridedam link'ą naująm post'ui
    return UL(rows)

def download():
    return response.download(request, db)

@auth.requires_login()  # reikalauja prisijungimo...
def notes():

    try: 
        post_id = int(request.args[0])  # pabandom gaut post'o ID (iš url)
    except:
        post_id = None  # jei ne - kursime naują post'ą

    note = request.vars.get('note')
    if note:
        db.chat.insert( 
            vardas=auth.user.first_name,
            zinute=note, 
            user_=auth.user.id, 
            post = post_id 
            )

        # isimink( note, , id=auth.user.id,  ) # vietoj 'user'
        # isimink( bot_note(note), 'bot', id=None )

    forma = FORM( "žn.:", 
             INPUT( _name="note", _type="text" ),
             BUTTON("siųsti", _type="submit")
          )
    return DIV( list_notes(), 
                forma, 
                A("išvalyti", _href="reset")
            )

def list_notes():

    try: 
        post_id = int(request.args[0])  # pabandom gaut post'o ID (iš url)
    except:
        post_id = None  # jei ne - kursime naują post'ą
    
    nice = []
    # for date, note in session.notes:
    rows =  db(db.chat.post==post_id).select( db.chat.ALL  ) # nuskaitom iš DB
    for row in rows:
        nice.append( 
            [
            SPAN(row.laikas.strftime("%Y-%m-%d %H:%M"), _name='date'), 
            " ",
            row.vardas, " ",
            B(row.zinute)
            ]
         )
    return UL(nice)

def reset():
    # hint: tiny.lt/pycrud
    db(db.chat).delete()
    redirect('notes')


# ---- example index page ----
def start():
    session.tikslas = randint(1, 10)
    session.spejimai = [ ]
    return A("eik spėliot..", _href="index")


def index():
    menu = "post list_posts notes list_notes user reset  speliones".split()
    return( UL( [A(x, _href=URL(x)) for x in menu] ) )

def speliones():
    if session.spejimai is None: 
        redirect('start')
        
    rez = FORM( "kiek:", INPUT( _name="kiek" ),
                 BUTTON("siųsti", _type="submit")
              )
    rez2 = "bandyk laimę.."
    if request.vars.kiek: # None, ""  veikia kaip Fasle
        kiek = int(request.vars.kiek)
        session.spejimai.append(kiek)#papildom spejimus
        if kiek == session.tikslas:
            rez2 = "Valio!"

    return  DIV(rez, BEAUTIFY(request.vars), 
                rez2, UL(session.spejimai))

def user():
    return dict(form=auth())