from random import randint
def index():
    return dict()
def start():
    session.tikslas = randint(1, 10)
    session.spejimai = [ ]
    return A("eik spėliot..", _href="index")

def data():
    if session.spejimai is None:
        redirect('start')
    if request.vars.q: session.m.append(request.vars.q)
    session.m.sort()
    rez = "bandyk laime"
    if request.vars.kiek:  # None, ""  veikia kaip Fasle
        kiek = int(request.vars.kiek)
        session.spejimai.append(kiek) # papildom spejimus
        if kiek < session.tikslas:
            rez = "daugiau"
        if kiek > session.tikslas:
            rez = "maziau"
        if kiek == session.tikslas:
            rez = "Valio!"
    return DIV(rez, BEAUTIFY(request.vars),
               UL(session.spejimai))
